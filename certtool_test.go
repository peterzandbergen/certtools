package certtools

import "testing"

func TestCorrectExponent(t *testing.T) {
	const expected = "AQAB"

	res := encodeExponent(65537)
	if res != expected {
		t.Errorf("expected: %s, got: %s", expected, res)
	}
}