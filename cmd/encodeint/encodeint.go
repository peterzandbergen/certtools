package main

import (
	"encoding/base64"
	"fmt"
)

func main() {
	b := []byte{1, 0, 1, 0}
	b64 := base64.StdEncoding.EncodeToString(b)
	fmt.Println(b64)
}
