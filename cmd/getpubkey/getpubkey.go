package main

import (
	"certtools"

	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func parseX509(b []byte) (*x509.Certificate, error) {
	cert, err := x509.ParseCertificate(b)
	if err != nil {
		return nil, err
	}
	return cert, nil
}

// loadPem loads a pem block from the io.Reader
func loadPem(r io.Reader) (*pem.Block, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	pb, _ := pem.Decode(b)
	if pb == nil {
		return nil, errors.New("loadPem: No block found")
	}
	return pb, nil
}

func loadPemFromFile(file string) (*pem.Block, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return loadPem(f)
}

func getRsaPublicKey(cert *x509.Certificate) (*rsa.PublicKey, error) {
	if cert.PublicKeyAlgorithm != x509.RSA {
		return nil, errors.New("need RSA public key")
	}
	var pub *rsa.PublicKey
	pub, ok := cert.PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("could not cast to RsaPubKey")
	}
	return pub, nil
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("filename argument missing")
		os.Exit(1)
	}
	p, err := loadPemFromFile(os.Args[1])
	if err != nil {
		fmt.Printf("main: error opening file: %s", err.Error())
		os.Exit(1)
	}
	// log.Printf("pem block found: %#v", p)

	cert, err := parseX509(p.Bytes)
	if err != nil {
		fmt.Printf("error parsing X509: %s", err.Error())
		os.Exit(1)
	}
	// log.Printf("%#v", cert)
	pub, err := getRsaPublicKey(cert)
	if err != nil {
		fmt.Printf("error getting pub key: %s", err.Error())
		os.Exit(1)
	}
	fmt.Printf("subject name: %s\n", cert.Subject.String())
	fmt.Printf("issuer name: %s\n", cert.Issuer.String())
	fmt.Printf("X509.SerialNumber: %s\n", cert.SerialNumber.String())
	pb64 := certtools.RSAPublicKey(*pub)
	fmt.Printf("RSAPubKey.modulus: %s\n", pb64.ModulusB64())
	fmt.Printf("RSAPubKey.exponent: %s\n", pb64.ExponentB64())
	return
}
