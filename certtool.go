package certtools

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
)

// RSAPublicKey adds base64 encoding methods to PublicKey.
type RSAPublicKey rsa.PublicKey

// ExponentB64 returns the base64 string of the exponent.
func (pk *RSAPublicKey) ExponentB64() string {
	return encodeExponent(pk.E)
}

func encodeExponent(i int) string {
	var be [4]byte
	binary.LittleEndian.PutUint32(be[:], uint32(i))
	return base64.StdEncoding.EncodeToString(be[:3])
}

// ModulusB64 returns the base64 representation of the modulus.
func (pk *RSAPublicKey) ModulusB64() string {
	return base64.StdEncoding.EncodeToString(pk.N.Bytes())
}
